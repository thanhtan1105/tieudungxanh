//
//  ProductPhotoCell.swift
//  TieuDungXanh
//
//  Created by Le Thanh Tan on 5/1/16.
//  Copyright © 2016 Le Thanh Tan. All rights reserved.
//

import UIKit

class ProductPhotoCell: UICollectionViewCell {

	@IBOutlet weak var photoImage: UIImageView!
	@IBOutlet weak var background: UIView!


	override func awakeFromNib() {
		self.layer.cornerRadius = 5
		self.clipsToBounds = true

		background.layer.cornerRadius = 5
		background.backgroundColor = UIColor.whiteColor()

		photoImage.layer.cornerRadius = 5
	}

	var product: Product? {
		didSet {
			photoImage.image = UIImage(named: (product?.photoImageName)!)
			if product!.isSelect {
				self.backgroundColor = UIColor(hex: 0x8dc540)
			}
		}
	}

}
