//
//  HeaderViewCollectionReusableView.swift
//  TieuDungXanh
//
//  Created by Le Thanh Tan on 5/2/16.
//  Copyright © 2016 Le Thanh Tan. All rights reserved.
//

import UIKit

class HeaderViewCollectionReusableView: UICollectionReusableView {

	@IBOutlet weak var label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
