//
//  ProductResultTableViewCell.swift
//  TieuDungXanh
//
//  Created by Le Thanh Tan on 5/15/16.
//  Copyright © 2016 Le Thanh Tan. All rights reserved.
//

import UIKit
import AVFoundation

protocol ProductResultTableViewDelegate: class {
	func productResultTableViewCell(productResultTableViewCell: UITableViewCell, didUpdateHeightCell height: CGFloat)
}

class ProductResultTableViewCell: UITableViewCell {

	@IBOutlet weak var collectionView: UICollectionView!
	weak var delegate: ProductResultTableViewDelegate!
	var productList: [Product]? {
		didSet {
			if let layout = collectionView.collectionViewLayout as? ProductLayout {
				layout.delegate = self
			}
			collectionView.delegate = self
			collectionView.dataSource = self
			collectionView.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: 10, right: 5)
			collectionView.reloadData()
		}
	}

	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code

	}

	override func setSelected(selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		// Configure the view for the selected state
	}
}

extension ProductResultTableViewCell: ProductLayoutDelegate {
	func collectionView(collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: NSIndexPath, withWidth width: CGFloat) -> CGFloat {
		if let productList = productList {
			let photoImage = productList[indexPath.item]
			let image = UIImage(named: photoImage.photoImageName)
			let boundingRect = CGRect(x: 0, y: 0, width: width, height: width)
			let rect = AVMakeRectWithAspectRatioInsideRect((image?.size)!, boundingRect)
			return rect.size.height
		}
		return 0
	}

	func collectionView(didPrepareLayout collectionView: UICollectionView) {
		let layout = collectionView.collectionViewLayout as! ProductLayout
		delegate.productResultTableViewCell(self, didUpdateHeightCell: layout.collectionViewContentSize().height)
	}
}

extension ProductResultTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ProductPhotoCell", forIndexPath: indexPath) as! ProductPhotoCell
		if let productList = productList {
			cell.product = productList[indexPath.row]
			if cell.product?.isSelect == cell.product?.isAnswear {
				cell.backgroundColor = UIColor(hex: 0x8dc540)
			} else {
				cell.backgroundColor = UIColor.clearColor()
			}
			return cell
		}
		return UICollectionViewCell()
	}

	func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
		return 1
	}

	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return productList == nil ? 0 : (productList?.count)!
	}

	func collectionView(collectionView: UICollectionView, didEndDisplayingCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
		cell.backgroundColor = UIColor.clearColor()
	}
}
