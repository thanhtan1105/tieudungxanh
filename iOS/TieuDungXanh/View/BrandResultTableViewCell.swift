//
//  BrandResultTableViewCell.swift
//  TieuDungXanh
//
//  Created by Le Thanh Tan on 5/15/16.
//  Copyright © 2016 Le Thanh Tan. All rights reserved.
//

import UIKit

class BrandResultTableViewCell: UITableViewCell {

	@IBOutlet weak var radioButton: UIButton!
	@IBOutlet weak var brandLabel: UILabel!

	var brand: Brand? {
		didSet {
			if !brand!.isAnswear {
				radioButton.setImage(UIImage(named: "radioButton"),
				                     forState: UIControlState.Normal)
			} else {
				radioButton.setImage(UIImage(named: "radioButtonSelect"),
				                     forState: UIControlState.Normal)
			}
			brandLabel.text = brand!.name
		}
	}

	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}

	override func setSelected(selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		// Configure the view for the selected state
	}

}
