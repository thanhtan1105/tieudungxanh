//
//  ManufacturerCollectionViewCell.swift
//  TieuDungXanh
//
//  Created by Le Thanh Tan on 5/1/16.
//  Copyright © 2016 Le Thanh Tan. All rights reserved.
//

import UIKit

class ManufacturerCell: UICollectionViewCell {
	
	@IBOutlet weak var photoImage: UIImageView!
	
	override func awakeFromNib() {
		self.layer.cornerRadius = 3
		self.clipsToBounds = true
		
		self.photoImage.layer.cornerRadius = 3
		self.photoImage.clipsToBounds = true
	}
	
	var manufacturer: Manufacturer? {
		didSet {
			photoImage.image = UIImage(named: (manufacturer?.photoImageName)!)
		}
	}		
}
