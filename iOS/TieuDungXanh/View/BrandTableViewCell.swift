//
//  BrandTableViewCell.swift
//  TieuDungXanh
//
//  Created by Le Thanh Tan on 5/14/16.
//  Copyright © 2016 Le Thanh Tan. All rights reserved.
//

import UIKit

protocol BrandTableViewCellDelegate: class {
	func brandTableViewCell(brandCell: BrandTableViewCell, didSelected selected: Bool)
}

class BrandTableViewCell: UITableViewCell {

	@IBOutlet weak var radioButton: UIImageView!
	@IBOutlet weak var brandLabel: UILabel!

	weak var delegate: BrandTableViewCellDelegate!
	var brand: Brand? {
		didSet {
			let imageName = brand!.isSelect ? "radioButtonSelect" : "radioButton"
//			radioButton.setImage(UIImage(named: imageName), forState: UIControlState.Normal)
			radioButton.image = UIImage(named: imageName)
			brandLabel.text = brand?.name
		}
	}

	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}

	override func setSelected(selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		// Configure the view for the selected state
	}

	@IBAction func onRadioButtonTapped(sender: UIButton) {
		print("Brand: \(brand?.name)")
		brand!.isSelect = !brand!.isSelect
		delegate.brandTableViewCell(self, didSelected: brand!.isSelect)
		let imageName = brand!.isSelect ? "radioButtonSelect" : "radioButton"
		radioButton.image = UIImage(named: imageName)
	}
}
