//
//  ProductLayout.swift
//  TieuDungXanh
//
//  Created by Le Thanh Tan on 4/30/16.
//  Copyright © 2016 Le Thanh Tan. All rights reserved.
//

import UIKit

protocol ProductLayoutDelegate: ProductLayoutDelegateOptional {
	func collectionView(collectionView: UICollectionView,
	                    heightForPhotoAtIndexPath indexPath: NSIndexPath,
																					withWidth width: CGFloat) -> CGFloat

}

protocol ProductLayoutDelegateOptional: class {
	func collectionView(didPrepareLayout collectionView: UICollectionView)
}

extension ProductLayoutDelegateOptional {
	func collectionView(didPrepareLayout collectionView: UICollectionView) {

	}
}

class ProductLayout: UICollectionViewLayout, UICollectionViewDelegateFlowLayout {
	weak var delegate: ProductLayoutDelegate!
	var numberOfColums = 2
	var cellPadding: CGFloat = 1.0
	let heightHeader: CGFloat = 30.0

	private var yOffsetMax = [CGFloat]()
	private var cache = [UICollectionViewLayoutAttributes]()
	private var contentHeight: CGFloat = 0.0
	private var contentWidth: CGFloat {
		let insets = collectionView?.contentInset
		return CGRectGetWidth(collectionView!.bounds) - (insets!.left + insets!.right)
	}

	override func prepareLayout() {
		if cache.isEmpty {
			let columnWidth = contentWidth / CGFloat(numberOfColums)
			var xOffset = [CGFloat]()
			for column in 0..<numberOfColums {
				xOffset.append(CGFloat(column) * columnWidth)
			}

			var column = 0
			var yOffset = [CGFloat](count: numberOfColums, repeatedValue: 0)
			yOffsetMax.append(0)
			if collectionView?.numberOfSections() > 1 {
				yOffset = [CGFloat](count: numberOfColums, repeatedValue: heightHeader)
			}

			let numberOfSections = collectionView!.numberOfSections()
			for section in 0..<numberOfSections {
				column = 0
				for item in 0..<collectionView!.numberOfItemsInSection(section) {
					let indexPath = NSIndexPath(forItem: item, inSection: section)

					let width = columnWidth - cellPadding * 2
					let photoHeight = delegate.collectionView(collectionView!,
					                                          heightForPhotoAtIndexPath: indexPath,
					                                          withWidth: width)
					let height = cellPadding + photoHeight
					let frame = CGRect(x: xOffset[column],
					                   y: yOffset[column],
					                   width: columnWidth,
					                   height: height)
					let insetFrame = frame.insetBy(dx: cellPadding, dy: cellPadding)

					let attributes = UICollectionViewLayoutAttributes(forCellWithIndexPath: indexPath)
					attributes.frame = insetFrame
					cache.append(attributes)

					contentHeight = max(contentHeight, frame.maxY)
					yOffset[column] = yOffset[column] + height

					if column >= (numberOfColums - 1) {
						column = 0
					} else {
						column += 1
					}
				}

				var maxValueYOffset: CGFloat = 0
				for value in yOffset {
					if value > maxValueYOffset {
						maxValueYOffset = value
					}
				}
				yOffsetMax.append(maxValueYOffset)

				// bonus height header of next section
				yOffset = [CGFloat](count: numberOfColums, repeatedValue: maxValueYOffset + heightHeader)
			}

			delegate.collectionView(didPrepareLayout: collectionView!)
		}
	}

	override func collectionViewContentSize() -> CGSize {
		return CGSize(width: contentWidth, height: contentHeight)
	}

	override func layoutAttributesForElementsInRect(rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
		var layoutAttributes = [UICollectionViewLayoutAttributes]()
		let sectionCount = collectionView!.dataSource!.numberOfSectionsInCollectionView!(collectionView!)

		for section in 0..<sectionCount {
			// add header
			layoutAttributes.append(self.layoutAttributesForSupplementaryViewOfKind(UICollectionElementKindSectionHeader, atIndexPath: NSIndexPath(forItem: 0, inSection: section))!)
		}

		for attributes in cache {
			if CGRectIntersectsRect(attributes.frame, rect) {
				layoutAttributes.append(attributes)
			}
		}

		return layoutAttributes
	}

	override func layoutAttributesForSupplementaryViewOfKind(elementKind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionViewLayoutAttributes? {

		let attributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: elementKind, withIndexPath: indexPath)
		attributes.frame = CGRect(x: 0, y: yOffsetMax[indexPath.section], width: (collectionView?.frame.width)! - 10, height: heightHeader)
		return attributes
	}

	func reloadLayout() {
		cache = [UICollectionViewLayoutAttributes]()
	}
}
