//
//  Question1ViewController.swift
//  TieuDungXanh
//
//  Created by Le Thanh Tan on 5/1/16.
//  Copyright © 2016 Le Thanh Tan. All rights reserved.
//

import UIKit
import AVFoundation

class Question1ViewController: UIViewController {

	@IBOutlet weak var tableView: UITableView!

	var brandList = Brand.allBrand()
	var selected = 0
	var step = 0
	var range = [0, 5, 10, 15]

	override func viewDidLoad() {
		self.navigationItem.setHidesBackButton(true, animated: false)
		let nib = UINib(nibName: "BrandTableViewCell", bundle: nil)
		tableView.registerNib(nib, forCellReuseIdentifier: "BrandTableViewCell")
		tableView.rowHeight = UITableViewAutomaticDimension
		tableView.estimatedRowHeight = 80.0
	}

	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		self.navigationController?.setNavigationBarHidden(false, animated: true)
		self.title = "Câu 1"

	}

	@IBAction func nextButtonTouchUpInside(sender: UIBarButtonItem) {
		step = step + 1
		guard step >= range.count else {
			selected = 0
			let animationOpaticy = CABasicAnimation(keyPath: "opacity")
			animationOpaticy.fromValue = 1.0
			animationOpaticy.toValue = 1.0
			tableView.layer.addAnimation(animationOpaticy, forKey: nil)
			tableView.reloadData()
			return
		}
	}

	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		if segue.identifier == "nextButtonQuestion1" {
			let viewController = segue.destinationViewController as! Question2ViewController
			var clientChoiceBrand = [Brand]()
			for brand in brandList {
				if brand.isSelect {
					clientChoiceBrand.append(brand)
				}
			}
			viewController.navigationItem.hidesBackButton = true
			viewController.brandList = clientChoiceBrand
		}
	}

	override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
		if identifier == "nextButtonQuestion1" {
			if selected == 0 {
				showAlertShouldNext()
				return false
			} else if step + 2 < range.count {
				nextButtonTouchUpInside(UIBarButtonItem())
				return false
			}
		}
		return true
	}

	private func showAlertVC() {
		let alertVC = UIAlertController(title: "Cảnh báo", message: "Quý khách chỉ được chọn tối đa 3 nhà sản xuất", preferredStyle: .Alert)
		let okButton = UIAlertAction(title: "OK", style: .Default) { (action: UIAlertAction) in

		}
		alertVC.addAction(okButton)
		self.presentViewController(alertVC, animated: true, completion: nil)
	}

	private func showAlertShouldNext() {
		let alertVC = UIAlertController(title: "Cảnh báo", message: "Quý khách nên chọn ít nhất 1 nhà sản xuất", preferredStyle: .Alert)
		let okButton = UIAlertAction(title: "OK", style: .Default) { (action: UIAlertAction) in
		}
		alertVC.addAction(okButton)
		self.presentViewController(alertVC, animated: true, completion: nil)
	}
}

extension Question1ViewController: UITableViewDelegate, UITableViewDataSource {
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
	}

	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		let count = range[step + 1] - range[step]
		return count
	}

	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("BrandTableViewCell") as! BrandTableViewCell
		let startIndex = range[step]
		cell.delegate = self
		cell.brand = brandList[startIndex + indexPath.row]
		return cell
	}

	func tableView(tableView: UITableView, didEndDisplayingCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
		let cell = tableView.cellForRowAtIndexPath(indexPath) as! BrandTableViewCell
		cell.radioButton.image = UIImage(named: "radioButton")
	}

	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		tableView.deselectRowAtIndexPath(indexPath, animated: true)
		let cell = tableView.cellForRowAtIndexPath(indexPath) as! BrandTableViewCell

		if cell.brand?.isSelect == false && selected == 3 {
			showAlertVC()
			return
		}
		cell.onRadioButtonTapped(UIButton())
	}
}

extension Question1ViewController: BrandTableViewCellDelegate {
	func brandTableViewCell(brandCell: BrandTableViewCell, didSelected selected: Bool) {
		if selected {
			self.selected += 1
		} else {
			self.selected -= 1
		}
	}
}
