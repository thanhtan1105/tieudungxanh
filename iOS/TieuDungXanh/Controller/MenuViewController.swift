//
//  MenuViewController.swift
//  TieuDungXanh
//
//  Created by Le Thanh Tan on 4/30/16.
//  Copyright © 2016 Le Thanh Tan. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
	
	@IBOutlet weak var image: UIImageView!
	@IBOutlet weak var startButton: UIButton!
	
	override func viewDidLoad() {
		startButton.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		self.navigationController?.setNavigationBarHidden(true, animated: false)
	}
	
	
}
