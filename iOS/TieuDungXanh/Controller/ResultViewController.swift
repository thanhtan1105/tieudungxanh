//
//  ResultViewController.swift
//  TieuDungXanh
//
//  Created by Le Thanh Tan on 5/1/16.
//  Copyright © 2016 Le Thanh Tan. All rights reserved.
//

import UIKit
import AVFoundation

class ResultViewController: UIViewController {

	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var resultLabel: UILabel!

	var productList = [Product]()
	var brandList = [Brand]()
	var contentSizeHeight: CGFloat?

	var numberCorrectBrand: Int {
		var count = 0
		for brand in brandList {
			if brand.isSelect == brand.isAnswear {
				count += 1
			}
		}
		return count
	}

	var numberCorrectProduct: Int {
		var count = 0
		for product in productList {
			if product.isSelect == product.isAnswear {
				count += 1
			}
		}
		return count
	}

	// MARK: - Public Function
	override func viewDidLoad() {
		super.viewDidLoad()
		self.navigationItem.setHidesBackButton(true, animated: false)
		resultLabel.text = "Tổng điểm: \(numberCorrectBrand + numberCorrectProduct)/20"

		let nib = UINib(nibName: "BrandResultTableViewCell", bundle: nil)
		tableView.registerNib(nib, forCellReuseIdentifier: "BrandResultTableViewCell")
		tableView.rowHeight = UITableViewAutomaticDimension
		tableView.estimatedRowHeight = 300.0
	}

	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		self.title = "Kết quả"
	}

	override func didReceiveMemoryWarning() {
			super.didReceiveMemoryWarning()
	}

	@IBAction func onHomeTouchUpInside(sender: UIBarButtonItem) {
		self.navigationController?.popToRootViewControllerAnimated(false)
	}
}

extension ResultViewController: UITableViewDelegate, UITableViewDataSource {
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		if indexPath.section == 0 {
			let cell = tableView.dequeueReusableCellWithIdentifier("BrandResultTableViewCell") as! BrandResultTableViewCell
			cell.brand = brandList[indexPath.row]
			return cell

		} else if indexPath.section == 1 {
			let cell = tableView.dequeueReusableCellWithIdentifier("ProductResultTableViewCell") as! ProductResultTableViewCell
			cell.productList = productList
			cell.delegate = self
			return cell
		}

		return UITableViewCell()
	}

	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 2
	}

	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return section == 0 ? brandList.count : 1
	}

	func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		var headerString = ""
		if section == 0 {
			let totalCorrectBrand = NSUserDefaults.standardUserDefaults().objectForKey("numberCorrectOfManufacturer") as! String
			headerString = "Câu hỏi 1: Chính xác \(numberCorrectBrand)/\(totalCorrectBrand)"
		} else {
			let totalCorrectProduct = NSUserDefaults.standardUserDefaults().objectForKey("numberCorrectOfProduct") as! String
			headerString = "Câu hỏi 2: Chính xác \(numberCorrectProduct)/\(totalCorrectProduct)"
		}
		return headerString
	}

	func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		if indexPath.section == 1 {
			return contentSizeHeight == nil ? 0 : contentSizeHeight!
		}

		return 50
	}
}

extension ResultViewController: ProductResultTableViewDelegate {
	func productResultTableViewCell(productResultTableViewCell: UITableViewCell, didUpdateHeightCell height: CGFloat) {
		tableView.beginUpdates()
		contentSizeHeight = height
		print("Height: \(contentSizeHeight)")
		tableView.endUpdates()
	}
}

// MARK - Private method
extension ResultViewController {

}
