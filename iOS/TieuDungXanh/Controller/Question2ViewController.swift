//
//  Question2ViewController.swift
//  TieuDungXanh
//
//  Created by Le Thanh Tan on 5/1/16.
//  Copyright © 2016 Le Thanh Tan. All rights reserved.
//

import UIKit
import AVFoundation

class Question2ViewController: UIViewController {

	var productList = Product.allProduct()
	var brandList = [Brand]()
	var selected = 0
	var step = 0
//	let range = [0, 3, 4, 9, 10, 15, 16, 21]
	let range = [0, 4, 10, 16, 22]

	@IBOutlet weak var collectionView: UICollectionView!

	override func viewDidLoad() {
		super.viewDidLoad()
		self.navigationItem.setHidesBackButton(true, animated: false)
		if let layout = collectionView.collectionViewLayout as? ProductLayout {
			layout.delegate = self
		}
		collectionView!.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: 10, right: 5)
	}

	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		self.title = "Câu 2"

	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}

	@IBAction func helloWorld(sender: UIBarButtonItem) {
		step = step + 1
		guard step >= range.count else {
			selected = 0
			let collectionViewAnimation = CABasicAnimation(keyPath: "opacity")
			collectionViewAnimation.fromValue = 0.0
			collectionViewAnimation.toValue = 1.0
			collectionViewAnimation.duration = 0.5
			collectionView.layer.addAnimation(collectionViewAnimation, forKey: nil)
			if let layout = collectionView.collectionViewLayout as? ProductLayout {
				layout.reloadLayout()
			}
			collectionView.reloadData()
			return
		}
	}

	private func showAlertVC() {
		let alertVC = UIAlertController(title: "Cảnh báo", message: "Quý khách chỉ được chọn tối đa 3 sản phẩm", preferredStyle: .Alert)
		let okButton = UIAlertAction(title: "OK", style: .Default) { (action: UIAlertAction) in

		}
		alertVC.addAction(okButton)
		self.presentViewController(alertVC, animated: true, completion: nil)
	}


	// MARK: - Navigation

	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		// Get the new view controller using segue.destinationViewController.
		// Pass the selected object to the new view controller.
		if segue.identifier == "nextButtonQuestion2" {
			let viewController = segue.destinationViewController as! ResultViewController
			var clientChoiceProduct = [Product]()
			for product in productList {
				if product.isSelect {
					clientChoiceProduct.append(product)
				}
			}
			viewController.navigationItem.hidesBackButton = true
			viewController.brandList = brandList
			viewController.productList = clientChoiceProduct
		}
	}

	override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
		if identifier == "nextButtonQuestion2" {
			if selected == 0 {
				showAlertShouldNext()
				return false
			} else if step + 2 < range.count {
				helloWorld(UIBarButtonItem())
				return false
			}
		}
		return true
	}

	private func showAlertShouldNext() {
		let alertVC = UIAlertController(title: "Cảnh báo", message: "Quý khách nên chọn ít nhất 1 sản phẩm", preferredStyle: .Alert)
		let okButton = UIAlertAction(title: "OK", style: .Default) { (action: UIAlertAction) in
		}
		alertVC.addAction(okButton)
		self.presentViewController(alertVC, animated: true, completion: nil)
	}

}

extension Question2ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
	func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
		return 1
	}

	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		let count = range[step + 1] - range[step]
		return count
	}

	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ProductPhotoCell", forIndexPath: indexPath) as! ProductPhotoCell
		let startIndex = range[step]
		cell.product = productList[startIndex + indexPath.row]
		return cell
	}

	func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
		let cell = collectionView.cellForItemAtIndexPath(indexPath) as! ProductPhotoCell
		if cell.product?.isSelect == false && selected == 3 {
			showAlertVC()

		} else {
			cell.product?.isSelect = !(cell.product?.isSelect)!
			if cell.product?.isSelect == true {
				cell.backgroundColor = UIColor(hex: 0x8dc540)
				selected += 1
			} else {
				cell.backgroundColor = UIColor.clearColor()
				selected -= 1
			}
		}
	}


	func collectionView(collectionView: UICollectionView, didEndDisplayingCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
		cell.backgroundColor = UIColor.clearColor()
	}
}

extension Question2ViewController: ProductLayoutDelegate {
	func collectionView(collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: NSIndexPath, withWidth width: CGFloat) -> CGFloat {
		let photoImage = productList[indexPath.item]
		let image = UIImage(named: photoImage.photoImageName)
		let boundingRect = CGRect(x: 0, y: 0, width: width, height: width)
		let rect = AVMakeRectWithAspectRatioInsideRect((image?.size)!, boundingRect)
		return rect.size.height
	}

}
