//
//  Brand.swift
//  TieuDungXanh
//
//  Created by Le Thanh Tan on 5/14/16.
//  Copyright © 2016 Le Thanh Tan. All rights reserved.
//

import Foundation

class Brand: NSObject {
	var name: String
	var isSelect: Bool
	var isAnswear: Bool

	class func allBrand() -> [Brand] {
		var brandList = [Brand]()
		if let URL = NSBundle.mainBundle().URLForResource("Brand", withExtension: "plist") {
			if let brandFromPlist = NSArray(contentsOfURL: URL) {
				for dictionary in brandFromPlist {
					let brand = Brand(dictionary: dictionary as! NSDictionary)
					brandList.append(brand)
				}
			}
		}
		return brandList
	}

	func toString() -> String {
		return "name: \(self.name), isSelect: \(isSelect), isAnswear: \(isAnswear)"
	}

	init(name: String, isAnswear: Bool) {
		self.name = name
		self.isSelect = false
		self.isAnswear = isAnswear
	}

	convenience init(dictionary: NSDictionary) {
		let name = dictionary["name"] as? String
		let isAnswear = (dictionary["isAnswear"] as? String)! == "0" ? false : true
		self.init(name: name!, isAnswear: isAnswear)
	}
}
