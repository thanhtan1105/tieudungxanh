//
//  Manufacturer.swift
//  TieuDungXanh
//
//  Created by Le Thanh Tan on 5/1/16.
//  Copyright © 2016 Le Thanh Tan. All rights reserved.
//

import Foundation

class Manufacturer: NSObject {
	var name: String
	var isSelect: Bool
	var isAnswear: Bool
	var photoImageName: String

	class func allManufacturer() -> [Manufacturer] {
		var manufacturerList = [Manufacturer]()
		if let URL = NSBundle.mainBundle().URLForResource("Manufacturer", withExtension: "plist") {
			if let manufacturerFromPlist = NSArray(contentsOfURL: URL) {
				for dictionary in manufacturerFromPlist {
					let manufacturer = Manufacturer(dictionary: dictionary as! NSDictionary)
					manufacturerList.append(manufacturer)
				}
			}
		}
		return manufacturerList
	}

	func toString() -> String {
		return "name: \(self.name), isSelect: \(isSelect), isAnswear: \(isAnswear), photoImageName: \(photoImageName)"
	}

	init(name: String, isAnswear: Bool, photoImageName: String) {
		self.name = name
		self.isSelect = false
		self.isAnswear = isAnswear
		self.photoImageName = photoImageName
	}

	convenience init(dictionary: NSDictionary) {
		let name = dictionary["name"] as? String
		let isAnswear = (dictionary["isAnswear"] as? String)! == "0" ? false : true
		let photoImageName = dictionary["imageName"] as? String
		self.init(name: name!, isAnswear: isAnswear, photoImageName: photoImageName!)
	}
}
