package com.coopmart.chiendichtieudungxanh.utils;

import com.coopmart.chiendichtieudungxanh.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by ManhNV on 5/11/2016.
 */
public class DataUtils {
    private static DataUtils INSTANCE=null;
    private HashMap<String,Boolean> FirstQuestionThumb;
    private ArrayList<String> FirstQuestionImageResourse;
    private HashMap<String,Boolean> FirstQuestionChecked;
    private HashMap<Integer,Boolean> SecondQuestionThumb;
    private ArrayList<Integer> SecondQuestionImageResourse;
    private HashMap<Integer,Boolean> SecondQuestionChecked;
    private DataUtils(){
        createFirstQuestion();
        createSecondQuestion();
        resetList();
    }

    public final int MAXFIRST=8;
    public final int MAXSECOND=3;

    private int ResultQuestionA;
    private int ResultQuestionB;

    public static DataUtils getINSTANCE(){
        if (INSTANCE==null){
            INSTANCE = new DataUtils();
        }
        return INSTANCE;
    }

    private void createFirstQuestion(){
        FirstQuestionImageResourse = new ArrayList<>();
        FirstQuestionThumb = new HashMap<String,Boolean>();
        FirstQuestionImageResourse.add("Công ty TNHH Unilever Việt Nam");
        FirstQuestionThumb.put(FirstQuestionImageResourse.get(FirstQuestionImageResourse.size()-1),true);
        FirstQuestionImageResourse.add("Công ty TNHH Colgate Palmolive Việt Nam");
        FirstQuestionThumb.put(FirstQuestionImageResourse.get(FirstQuestionImageResourse.size()-1),true);
        FirstQuestionImageResourse.add("Công ty Dầu gội Sữa tắm Romano");
        FirstQuestionThumb.put(FirstQuestionImageResourse.get(FirstQuestionImageResourse.size()-1),false);
        FirstQuestionImageResourse.add("Liên hiệp HTX Thương mại TP.HCM Saigon Co.op");
        FirstQuestionThumb.put(FirstQuestionImageResourse.get(FirstQuestionImageResourse.size()-1),true);
        FirstQuestionImageResourse.add("Công ty CP Acecook Việt Nam");
        FirstQuestionThumb.put(FirstQuestionImageResourse.get(FirstQuestionImageResourse.size()-1),false);


        FirstQuestionImageResourse.add("Công ty CPHH Vedan Việt Nam");
        FirstQuestionThumb.put(FirstQuestionImageResourse.get(FirstQuestionImageResourse.size()-1),false);
        FirstQuestionImageResourse.add("Công ty CP Bao Bì VAFACO");
        FirstQuestionThumb.put(FirstQuestionImageResourse.get(FirstQuestionImageResourse.size()-1),true);
        FirstQuestionImageResourse.add("Công ty CP Thực phẩm Cholimex");
        FirstQuestionThumb.put(FirstQuestionImageResourse.get(FirstQuestionImageResourse.size()-1),false);
        FirstQuestionImageResourse.add("Công ty CP Thực phẩm Á Châu");
        FirstQuestionThumb.put(FirstQuestionImageResourse.get(FirstQuestionImageResourse.size()-1),false);
        FirstQuestionImageResourse.add("Công ty CP Lương Thực Thực Phẩm Colusa – Miliket");
        FirstQuestionThumb.put(FirstQuestionImageResourse.get(FirstQuestionImageResourse.size()-1),true);



        FirstQuestionImageResourse.add("Nhà máy Bia Sài Gòn - Quận 5");
        FirstQuestionThumb.put(FirstQuestionImageResourse.get(FirstQuestionImageResourse.size()-1),true);
        FirstQuestionImageResourse.add("Tập đoàn Tân Hiệp Phát");
        FirstQuestionThumb.put(FirstQuestionImageResourse.get(FirstQuestionImageResourse.size()-1),false);
        FirstQuestionImageResourse.add("Nhà máy Sữa Sài Gòn - Củ Chi");
        FirstQuestionThumb.put(FirstQuestionImageResourse.get(FirstQuestionImageResourse.size()-1),true);
        FirstQuestionImageResourse.add("Nhà máy Sữa Trường Thọ - Thủ Đức");
        FirstQuestionThumb.put(FirstQuestionImageResourse.get(FirstQuestionImageResourse.size()-1),true);
        FirstQuestionImageResourse.add("Công ty CP Hóa chất Vi sinh Xí nghiệp Hương Việt");
        FirstQuestionThumb.put(FirstQuestionImageResourse.get(FirstQuestionImageResourse.size()-1),false);


        FirstQuestionChecked = new HashMap<String,Boolean>();
    }

    private void createSecondQuestion(){
        SecondQuestionThumb = new HashMap<Integer,Boolean>();
        //SecondQuestionThumb.put(R.drawable.rejoice,false);

        SecondQuestionImageResourse = new ArrayList<Integer>();
        SecondQuestionImageResourse.add(R.drawable.aa);
        SecondQuestionThumb.put(SecondQuestionImageResourse.get(SecondQuestionImageResourse.size() - 1), true);
        SecondQuestionImageResourse.add(R.drawable.abw);
        SecondQuestionThumb.put(SecondQuestionImageResourse.get(SecondQuestionImageResourse.size() - 1), false);
        SecondQuestionImageResourse.add(R.drawable.ac);
        SecondQuestionThumb.put(SecondQuestionImageResourse.get(SecondQuestionImageResourse.size() - 1), true);
        SecondQuestionImageResourse.add(R.drawable.ad);
        SecondQuestionThumb.put(SecondQuestionImageResourse.get(SecondQuestionImageResourse.size() - 1), true);
        SecondQuestionImageResourse.add(R.drawable.ba);
        SecondQuestionThumb.put(SecondQuestionImageResourse.get(SecondQuestionImageResourse.size() - 1), true);
        SecondQuestionImageResourse.add(R.drawable.bbw);
        SecondQuestionThumb.put(SecondQuestionImageResourse.get(SecondQuestionImageResourse.size() - 1), false);
        SecondQuestionImageResourse.add(R.drawable.be);
        SecondQuestionThumb.put(SecondQuestionImageResourse.get(SecondQuestionImageResourse.size() - 1), true);
        SecondQuestionImageResourse.add(R.drawable.bgw);
        SecondQuestionThumb.put(SecondQuestionImageResourse.get(SecondQuestionImageResourse.size() - 1), false);
        SecondQuestionImageResourse.add(R.drawable.bc);
        SecondQuestionThumb.put(SecondQuestionImageResourse.get(SecondQuestionImageResourse.size() - 1), true);
        SecondQuestionImageResourse.add(R.drawable.bdw);
        SecondQuestionThumb.put(SecondQuestionImageResourse.get(SecondQuestionImageResourse.size() - 1), false);
        SecondQuestionImageResourse.add(R.drawable.ca);
        SecondQuestionThumb.put(SecondQuestionImageResourse.get(SecondQuestionImageResourse.size() - 1), true);
        SecondQuestionImageResourse.add(R.drawable.cb);
        SecondQuestionThumb.put(SecondQuestionImageResourse.get(SecondQuestionImageResourse.size() - 1), true);
        SecondQuestionImageResourse.add(R.drawable.cfw);
        SecondQuestionThumb.put(SecondQuestionImageResourse.get(SecondQuestionImageResourse.size() - 1), false);
        SecondQuestionImageResourse.add(R.drawable.cd);
        SecondQuestionThumb.put(SecondQuestionImageResourse.get(SecondQuestionImageResourse.size() - 1), true);
        SecondQuestionImageResourse.add(R.drawable.cc);
        SecondQuestionThumb.put(SecondQuestionImageResourse.get(SecondQuestionImageResourse.size() - 1), true);
        SecondQuestionImageResourse.add(R.drawable.cew);
        SecondQuestionThumb.put(SecondQuestionImageResourse.get(SecondQuestionImageResourse.size() - 1), false);
        SecondQuestionImageResourse.add(R.drawable.da);
        SecondQuestionThumb.put(SecondQuestionImageResourse.get(SecondQuestionImageResourse.size() - 1), true);
        SecondQuestionImageResourse.add(R.drawable.dew);
        SecondQuestionThumb.put(SecondQuestionImageResourse.get(SecondQuestionImageResourse.size() - 1), false);
        SecondQuestionImageResourse.add(R.drawable.dcw);
        SecondQuestionThumb.put(SecondQuestionImageResourse.get(SecondQuestionImageResourse.size() - 1), false);
        SecondQuestionImageResourse.add(R.drawable.db);
        SecondQuestionThumb.put(SecondQuestionImageResourse.get(SecondQuestionImageResourse.size() - 1), true);
        SecondQuestionImageResourse.add(R.drawable.dd);
        SecondQuestionThumb.put(SecondQuestionImageResourse.get(SecondQuestionImageResourse.size() - 1), true);
        SecondQuestionImageResourse.add(R.drawable.dfw);
        SecondQuestionThumb.put(SecondQuestionImageResourse.get(SecondQuestionImageResourse.size() - 1), false);
        SecondQuestionChecked = new HashMap<Integer,Boolean>();
    }

    public void resetList(){
//        Collections.shuffle(FirstQuestionImageResourse);
//        Collections.shuffle(SecondQuestionImageResourse);
        FirstQuestionChecked.clear();
        SecondQuestionChecked.clear();
        ResultQuestionA=0;
        ResultQuestionB=0;
    }

    public ArrayList<String> getFirstQuestionImageResourse() {
        return FirstQuestionImageResourse;
    }

    public ArrayList<Integer> getSecondQuestionImageResourse() {
        return SecondQuestionImageResourse;
    }

    public void setFirstQuestionChecked(int position ,boolean flag){
        String resource = FirstQuestionImageResourse.get(position);
        if (FirstQuestionChecked.containsKey(resource)){
            FirstQuestionChecked.remove(resource);
        }else{
            if (flag) {
                if (FirstQuestionThumb.get(resource)) {
                    FirstQuestionChecked.put(resource, true);
                }else{
                    FirstQuestionChecked.put(resource, false);
                }
            }
        }
    }
    public void setSecondQuestionChecked(int position,int count){
        int resource = SecondQuestionImageResourse.get(position);
        if (SecondQuestionChecked.containsKey(resource)){
            SecondQuestionChecked.remove(resource);
        }else{

            if (count<MAXSECOND ) {
                if (SecondQuestionThumb.get(resource)) {
                    SecondQuestionChecked.put(resource, true);
                } else {
                    SecondQuestionChecked.put(resource,false);
                }
            }

        }
    }

    public void checkAnswerFirstQuestion(){
        int count=0;

        ResultQuestionA = FirstQuestionChecked.size();
    }
    public void checkAnswerSecondQuestion(){
        int count=0;

        ResultQuestionB = SecondQuestionChecked.size();
    }

    public int getResultQuestionA() {
        return ResultQuestionA;
    }

    public int getResultQuestionB() {
        return ResultQuestionB;
    }

    public HashMap<String,Boolean> getListResultFirst(){
        return FirstQuestionChecked;
    }
    public HashMap<Integer,Boolean> getListResultSecond(){
        return SecondQuestionChecked;
    }


}
