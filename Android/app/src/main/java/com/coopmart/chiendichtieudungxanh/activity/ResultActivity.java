package com.coopmart.chiendichtieudungxanh.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coopmart.chiendichtieudungxanh.R;
import com.coopmart.chiendichtieudungxanh.adapter.ImagesAdapter;
import com.coopmart.chiendichtieudungxanh.custom.MyGridView;
import com.coopmart.chiendichtieudungxanh.utils.DataUtils;
import com.etsy.android.grid.StaggeredGridView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ManhNV on 5/13/2016.
 */
public class ResultActivity extends AppCompatActivity {
    private MyGridView mGridView1;
    private ImagesAdapter mAdapter1;
    private StaggeredGridView mGridView2;
    private ImagesAdapter mAdapter2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_result);
        toolbar.setTitle("Kết quả");
        setSupportActionBar(toolbar);
        TextView limit_first = (TextView) findViewById(R.id.limit_first);
        limit_first.setText(" / " + DataUtils.getINSTANCE().MAXFIRST);
        TextView limit_second = (TextView) findViewById(R.id.limit_second);
        limit_second.setText(" / "+DataUtils.getINSTANCE().MAXSECOND*4);
        LinearLayout layout = (LinearLayout) findViewById(R.id.layout_result_first);
        HashMap<String,Boolean> ListResultFirst = DataUtils.getINSTANCE().getListResultFirst();
        String s="";
        int resultFirst=0;
        for (String item: ListResultFirst.keySet()) {
            TextView textView = new TextView(this);
            textView.setText("- "+item);
            textView.setTextSize(20);
            textView.setTextColor(Color.BLACK);
            //textView.setPadding(10,0,0,0);
            if (ListResultFirst.get(item)) {
                resultFirst++;
                textView.setTypeface(null, Typeface.BOLD);
            }else{
                textView.setTypeface(null, Typeface.NORMAL);
            }
            layout.addView(textView);
        }

//        mGridView1 = (MyGridView) findViewById(R.id.grid_view_result1);

//        mAdapter1 = new ImagesAdapter(this,android.R.layout.simple_list_item_1,ListResultFirst );
//        mGridView1.setAdapter(mAdapter1);

        HashMap<Integer,Boolean> ListResultSecond = DataUtils.getINSTANCE().getListResultSecond();
        mGridView1 = (MyGridView) findViewById(R.id.grid_view_result2);

        List<Integer> listResult = new ArrayList<Integer>(DataUtils.getINSTANCE().getListResultSecond().keySet());
        mAdapter2 = new ImagesAdapter(this,android.R.layout.simple_list_item_1, listResult);
        mAdapter2.setmChecked(DataUtils.getINSTANCE().getListResultSecond());
        mAdapter2.notifyDataSetChanged();
        mGridView1.setAdapter(mAdapter2);
        int a = resultFirst;
        TextView result1 = (TextView) findViewById(R.id.tv_result1);
        result1.setText(a + "");
        int b= mAdapter2.getNumOfChecked();
        TextView result2 = (TextView) findViewById(R.id.tv_result2);
        result2.setText(b+"");
        TextView result = (TextView) findViewById(R.id.tv_result);
        int sum =a+b;
        result.setText("Tổng điểm: "+sum +"/"+(DataUtils.getINSTANCE().MAXFIRST+(DataUtils.getINSTANCE().MAXSECOND*4)));
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_result, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id==R.id.mn_home){
            Intent intent = new Intent(ResultActivity.this,MainActivity.class);
            startActivity(intent);
            finish();
        }
        return true;
    }

    @Override
    public void onBackPressed() {

    }
}
