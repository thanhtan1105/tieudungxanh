package com.coopmart.chiendichtieudungxanh.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.coopmart.chiendichtieudungxanh.R;
import com.coopmart.chiendichtieudungxanh.adapter.FirstQuestionAdapter;
import com.coopmart.chiendichtieudungxanh.utils.DataUtils;
import com.etsy.android.grid.StaggeredGridView;

import java.util.ArrayList;
import java.util.List;

public class FirstQuestionActivity extends AppCompatActivity  {
    private StaggeredGridView mGridView;
    private FirstQuestionAdapter mAdapter;
    private int startPosition;
    private int endPosition;
    private List<String> mListProducer;
    private List<String> mListProducerChird;
    private int MAX=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_question);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_first);
        toolbar.setTitle("Câu 1");
        setSupportActionBar(toolbar);
        mListProducer = DataUtils.getINSTANCE().getFirstQuestionImageResourse();
        mListProducerChird = new ArrayList<String>();
        ListView lv = (ListView) findViewById(R.id.lv_first);
         mAdapter = new FirstQuestionAdapter(this,android.R.layout.simple_list_item_1,mListProducerChird);
        startPosition = 0;
        endPosition=4;
        MAX=3;
        resetChirdList();
        mAdapter.setList(mListProducerChird,startPosition,MAX);
        lv.setAdapter(mAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

               // Toast.makeText(FirstQuestionActivity.this, position +" Clicked", Toast.LENGTH_SHORT).show();
//                DataUtils.getINSTANCE().setFirstQuestionChecked(position,mAdapter.getNumOfChecked());
//                mAdapter.setChecked(position);
//                mAdapter.notifyDataSetChanged();
            }
        });
//        mGridView = (StaggeredGridView) findViewById(R.id.grid_view_first);
//        mAdapter = new ImagesAdapter(this,android.R.layout.simple_list_item_1, DataUtils.getINSTANCE().getFirstQuestionImageResourse());
//        mGridView.setAdapter(mAdapter);
//        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                //Toast.makeText(FirstQuestionActivity.this, "Item Clicked: " + position, Toast.LENGTH_SHORT).show();
//                //mAdapter.setmSelectedItem(position);
//                DataUtils.getINSTANCE().setFirstQuestionChecked(position,mAdapter.getNumOfChecked());
//                    mAdapter.setChecked(position);
//                    mAdapter.notifyDataSetChanged();
//            }
//        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.nextquestion, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id==R.id.mn_next_question){
            if (mAdapter.getNumOfChecked()>0){
                startPosition = endPosition+1;
                switch (startPosition){
                    case 5: endPosition=9;
                        MAX=3;
                        break;
                    case 10:
                        endPosition = 14;
                        MAX=3;
                        break;
                }
                if (startPosition>=mListProducer.size()) {
                    DataUtils.getINSTANCE().checkAnswerFirstQuestion();
                    //   Toast.makeText(FirstQuestionActivity.this, DataUtils.getINSTANCE().getResultQuestionA()+" Đáp án đúng", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(FirstQuestionActivity.this, SecondQuestionActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    resetChirdList();
                    mAdapter.setList(mListProducerChird,startPosition,MAX);
                }
            }else{
                Toast.makeText(FirstQuestionActivity.this, "Quý khách cần chọn ít nhất 1 nhà sản xuất", Toast.LENGTH_SHORT).show();
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {

    }

    private void resetChirdList(){
        mListProducerChird.clear();
        for (int i=startPosition;i<=endPosition;i++){
            mListProducerChird.add(mListProducer.get(i));
        }
    }
}
