package com.coopmart.chiendichtieudungxanh.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.coopmart.chiendichtieudungxanh.R;
import com.coopmart.chiendichtieudungxanh.adapter.ImagesAdapter;
import com.coopmart.chiendichtieudungxanh.utils.DataUtils;
import com.etsy.android.grid.StaggeredGridView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ManhNV on 5/13/2016.
 */
public class SecondQuestionActivity extends AppCompatActivity {
    private StaggeredGridView mGridView;
    private ImagesAdapter mAdapter;
    private int startPosition = 0;
    private int endPosition=0;

    private List<Integer> mListImage;
    private List<Integer> mListImageChird;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_question);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_second);
        toolbar.setTitle("Câu 2");
        setSupportActionBar(toolbar);
        mListImage = DataUtils.getINSTANCE().getSecondQuestionImageResourse();
        mListImageChird = new ArrayList<Integer>();
        mGridView = (StaggeredGridView) findViewById(R.id.grid_view_second);
        mAdapter = new ImagesAdapter(this,android.R.layout.simple_list_item_1, mListImageChird);
        startPosition = 0;
        endPosition=3;

        resetChirdList();
        mAdapter.setList(mListImageChird);
        mGridView.setAdapter(mAdapter);
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(FirstQuestionActivity.this, "Item Clicked: " + position, Toast.LENGTH_SHORT).show();
                //mAdapter.setmSelectedItem(position);
                DataUtils.getINSTANCE().setSecondQuestionChecked(position+startPosition,mAdapter.getNumOfChecked());
                    mAdapter.setChecked(position);
                    mAdapter.notifyDataSetChanged();
            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.nextquestion, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id==R.id.mn_next_question){
            if (mAdapter.getmChecked().size()>0){
                startPosition = endPosition+1;
                switch (startPosition){
                    case 4: endPosition=9;
                        break;
                    case 10:
                        endPosition = 15;
                        break;
                    case 16:
                        endPosition = 21;
                }
                if (startPosition>=DataUtils.getINSTANCE().getSecondQuestionImageResourse().size()) {
                    DataUtils.getINSTANCE().checkAnswerSecondQuestion();
                    //   Toast.makeText(SecondQuestionActivity.this, DataUtils.getINSTANCE().getResultQuestionB() + " Đáp án đúng", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(SecondQuestionActivity.this, ResultActivity.class);
                    startActivity(intent);
                    finish();
                }else{

                resetChirdList();
                    mAdapter.setList(mListImageChird);
//                    Toast.makeText(SecondQuestionActivity.this, "Bắt đầu:" +startPosition+". Kết thúc:"+endPosition, Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(SecondQuestionActivity.this, "Quý khách cần chọn ít nhất 1 sản phẩm", Toast.LENGTH_SHORT).show();
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {

    }

    private void resetChirdList(){
        mListImageChird.clear();
        for (int i=startPosition;i<=endPosition;i++){
            mListImageChird.add(mListImage.get(i));
        }
    }
}
