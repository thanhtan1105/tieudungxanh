package com.coopmart.chiendichtieudungxanh.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;

import com.coopmart.chiendichtieudungxanh.R;
import com.coopmart.chiendichtieudungxanh.application.AnalyticsApplication;
import com.coopmart.chiendichtieudungxanh.utils.DataUtils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class MainActivity extends Activity {

    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Main Menu");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        ImageButton btnStart = (ImageButton) findViewById(R.id.btn_start);
        btnStart.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ImageButton ib = (ImageButton) v;
                if (event.getAction() == MotionEvent.ACTION_DOWN){
                    ((ImageButton) v).setImageAlpha(150);
                 //   v.setBackgroundColor(Color.parseColor("#94ff00"));
                }else if (event.getAction() == MotionEvent.ACTION_UP){

                    ((ImageButton) v).setImageAlpha(255);
//                    v.setBackgroundColor(Color.parseColor("#ffffff"));
                }
                return false;
            }
        });

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageButton ib = (ImageButton) v;
                ((ImageButton) v).setImageAlpha(150);
                DataUtils.getINSTANCE().resetList();
                Intent intent = new Intent(MainActivity.this, FirstQuestionActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }
}
