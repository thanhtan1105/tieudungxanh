package com.coopmart.chiendichtieudungxanh.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.coopmart.chiendichtieudungxanh.R;
import com.coopmart.chiendichtieudungxanh.utils.DataUtils;
import com.etsy.android.grid.util.DynamicHeightImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ManhNV on 5/13/2016.
 */
public class ImagesAdapter extends ArrayAdapter<Integer> {


    private final Context mContext;
    private List<Integer> mImages;
    private HashMap<Integer,Boolean> mChecked;
    private int maxSelection;

    public ImagesAdapter(Context context, int resource, List<Integer> images) {
        super(context, resource, images);
        this.mContext = context;
        this.mImages = images;
        mChecked = new HashMap<Integer,Boolean>();
    }

    @Override
    public int getCount() {
        return mImages.size();
    }

    @Override
    public Integer getItem(int position) {
        return mImages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mImages.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView==null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.gridview_item,parent,false);
        }
        int resource = getItem(position);
        DynamicHeightImageView imageView = (DynamicHeightImageView) convertView.findViewById(R.id.imgView );
        imageView.setHeightRatio(getRatioHeight(getItem(position)));
        imageView.setImageResource(resource);
        FrameLayout layout = (FrameLayout) convertView.findViewById(R.id.panel_content);
        if (mChecked.containsKey(resource) && mChecked.get(resource)){
            layout.setBackgroundResource(R.drawable.border_image);
        }else{
            layout.setBackgroundColor(0);
        }
        return convertView;
    }

    private double getRatioHeight(Integer Resource){
//        BitmapFactory.Options dimensions = new BitmapFactory.Options();
//        dimensions.inJustDecodeBounds = true;
//        Bitmap mBitmapB = BitmapFactory.decodeResource(mContext.getResources(), Resource, dimensions);
//        double height = dimensions.outHeight;
//        double width =  dimensions.outWidth;
//        double ratio = height/width;
//        return ratio;//>0?ratio:height/width;
        return 1.0;
    }

    public void setChecked(int position){
        int resource = getItem(position);

        if (mChecked.containsKey(resource)){
            mChecked.remove(resource);
        }else{
            if (getNumOfChecked()<DataUtils.getINSTANCE().MAXSECOND) {
                mChecked.put(resource, true);
            }
            else{
                Toast.makeText(mContext, String.format("Quý khách chỉ được chọn tối đa %d sản phẩm",DataUtils.getINSTANCE().MAXSECOND), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public int getNumOfChecked(){
        int count =0;
        for (int item:mChecked.keySet()
             ) {
            if (mChecked.get(item)){
                count++;
            }
        }
        return count;
    }

    public HashMap<Integer, Boolean> getmChecked() {
        return mChecked;
    }

    public void setmChecked(HashMap<Integer,Boolean> result){
        mChecked=result;
        notifyDataSetChanged();
    }

    public void setList(List<Integer> listImage){
        this.mImages = listImage;
        mChecked.clear();
        mChecked = new HashMap<Integer, Boolean>();

        notifyDataSetChanged();
    }
}
