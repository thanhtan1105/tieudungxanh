package com.coopmart.chiendichtieudungxanh.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Toast;

import com.coopmart.chiendichtieudungxanh.R;
import com.coopmart.chiendichtieudungxanh.utils.DataUtils;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ManhNV on 5/14/2016.
 */
public class FirstQuestionAdapter extends ArrayAdapter<String> {
    private Context mContext;
    private List<String> mText;
    private HashMap<String,Boolean> mChecked;
    private int startPosition=0;
    private int max=0;


    public FirstQuestionAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
        mContext = context;
        mText = objects;
        mChecked = new HashMap<>();
    }
    @Override
    public int getCount() {
        return mText.size();
    }

    @Override
    public String getItem(int position) {
        return mText.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mText.get(position).hashCode();
    }



    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView==null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listview_item,parent,false);
        }
          final CheckBox  mCb;
        mCb = (CheckBox) convertView.findViewById(R.id.item_checkbox);
        String resource = getItem(position);
        mCb.setText(getItem(position));
        if (mChecked.containsKey(resource)){
            mCb.setChecked(true);
            mCb.setTypeface(null, Typeface.BOLD);

        }else{
            mCb.setChecked(false);
            mCb.setTypeface(null, Typeface.NORMAL);

        }
        mCb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;

                    DataUtils.getINSTANCE().setFirstQuestionChecked(position+startPosition, getNumOfChecked()<max);
                    String resource = getItem(position);

                    if (mChecked.containsKey(resource)) {
                        mChecked.remove(resource);
                        mCb.setTypeface(null, Typeface.NORMAL);

                    } else {
                        if (getNumOfChecked()<max) {
                        mChecked.put(resource, true);
                        mCb.setTypeface(null, Typeface.BOLD);
                        }else{
                            Toast.makeText(mContext, String.format("Quý khách chỉ được chọn tối đa %d nhà sản xuất",max), Toast.LENGTH_SHORT).show();
                            mCb.setChecked(false);
                        }
                    }


                //Toast.makeText(mContext , position +" Clicked", Toast.LENGTH_SHORT).show();
            }
        });

        return convertView;
    }


    public int getNumOfChecked(){
        return mChecked.size();
    }

    public void setList(List<String> list,int start,int max){

        this.max = max;
        startPosition=start;
        mText=list;
        mChecked.clear();
        mChecked = new HashMap<String, Boolean>();
        notifyDataSetChanged();
    }
}
